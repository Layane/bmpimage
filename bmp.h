#ifndef _BMP_H_
#define _BMP_H_

struct __attribute__((packed))
  bmp_header {
  uint8_t bfType[2]; // 0x42
  uint32_t bfileSize; // 54(What means the size of the image header + info header) + 4(multiple of 4 please) * size of the image  ex; 54 + 4 *(1024 X 2000)
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression; // no necessary
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter; // 72ppi
  uint32_t biyPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};




#endif
