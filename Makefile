CC = gcc
CFLAGS = -std=c11 -ggdb -Wall
LD = gcc

all: main

main : main.o
	$(LD) $< -o $@

main.o : bmp.c bmp.h
	$(CC) $(CFLAGS) -c $< -o $@

clean: 
	rm main.o main *.bmp


.PHONY : clean
