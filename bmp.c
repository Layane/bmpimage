#include <stdio.h>
#include <stdint.h>
#include <malloc.h>
#include "bmp.h"



#define BMP_TYPE_1 0x42
#define BMP_TYPE_2 0x4D
#define BMP_RESERVED 0
#define BMP_INFO_SIZE 40
#define BMP_INFO_PLANES 1
#define BMP_INFO_BIT_COUNT 24
#define BMP_INFO_COMPRESSION 0
#define BMP_INFO_X_PIXELS_PER_METER 2835
#define BMP_INFO_Y_PIXELS_PER_METER 2835
#define BMP_INFO_COLOUR_PALETTE 0
#define BMP_INFO_COLOUR_IMPORTANT 0


struct pixel {uint8_t b, g, r;};

struct image{
  uint32_t width, height;
  struct pixel* array;
};

enum read_status {
  READ_OK = 0,
  READ_ERROR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
};

enum write_status{
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_INVALID_HEADER,
  WRITE_ERROR_BISIZE,
};

enum read_pixel{
  READ_PIXEL_OK = 0,
  READ_PIXEL_ERROR,
  READ_ERROR_SIZE,
};

enum write_pixel{
  WRITE_PIXEL_OK = 0,
  WRITE_PIXEL_ERROR,
  WRITE_ERROR_SIZE,
};

struct bmp_header* header(uint32_t width, uint32_t height) {
  struct bmp_header* header= malloc(sizeof(*header));
  uint32_t imagebisize = width * height;
  uint32_t imgsize = imagebisize * 4;

  uint32_t file_size = sizeof(struct bmp_header) + imgsize;

  header->bfType[0] = BMP_TYPE_1;
  header->bfType[1] = BMP_TYPE_2;
  header->bfileSize = file_size;
  header->bfReserved = 0;
  header->bOffBits = sizeof(struct bmp_header);
  header->biSize = BMP_INFO_SIZE;
  header->biWidth = width;
  header->biHeight = height;
  header->biPlanes = BMP_INFO_PLANES;
  header->biBitCount = BMP_INFO_BIT_COUNT;
  header->biCompression = BMP_INFO_COMPRESSION;
  header->biSizeImage = imagebisize;
  header->biXPelsPerMeter= BMP_INFO_X_PIXELS_PER_METER;
  header->biyPelsPerMeter= BMP_INFO_Y_PIXELS_PER_METER;
  header->biClrUsed = BMP_INFO_COLOUR_PALETTE ;
  header->biClrImportant= BMP_INFO_COLOUR_IMPORTANT;

 return header;

}

struct pixel* define_colour(uint8_t blue, uint8_t green, uint8_t red){
  struct pixel* px = malloc(sizeof(*px));
  px->b = blue;
  px->g = green;
  px->r = red;

  return px;

}

enum write_status to_image_bmp(FILE* out, struct bmp_header* const write){

  if (write->biSizeImage < 4) return WRITE_ERROR_BISIZE;


  if(!fwrite(write, sizeof(struct bmp_header), 1, out)) return WRITE_ERROR;

  return WRITE_OK;

}

enum read_status from_image_bmp(FILE* in, struct bmp_header* const read){

  if (!(read->bfType[0] == 0x42) && (read->bfType[1] == 0x4D)) return READ_INVALID_SIGNATURE;


  return READ_OK;
}



enum write_pixel colour_image(FILE* out, struct pixel* px) {
  if(!fwrite(px, sizeof(struct pixel), 1, out)) return WRITE_PIXEL_ERROR;

  return WRITE_PIXEL_OK;

}


void fill_pixel(FILE* out, int* error, struct image* img){
  uint32_t x, y;
  struct pixel* px = img->array;
  for(y = 0; y < img->height; y++)
    for(x = 0; x < img->width; x++)
      if(!colour_image(out, px))  *error= 1;


  *error = 0;


}


int main(int argc, char const *argv[]) {
  // create a main file and run it!
  // A file .bmp will be created
  // so... type xxd img.bmp in terminal

  struct image* img = malloc(sizeof(*img));
  int error;
  img->height = 200;
  img->width = 200;
  struct bmp_header* head = header(img->width, img->height);


  FILE* out = fopen("img.bmp", "wb");

  FILE* in = fopen("img.bmp", "rb");

  printf("%d\n", to_image_bmp(out, head));




  struct pixel* px = define_colour(255, 255, 0);

  img->array = px;

  fill_pixel(out, &error, img);
  printf("%d\n", error);
  free(px);
  free(img);
  fclose(in);

}
